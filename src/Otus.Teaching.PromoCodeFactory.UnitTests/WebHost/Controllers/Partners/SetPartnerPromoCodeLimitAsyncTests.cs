﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using Xunit;
using FluentAssertions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, null);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partner = new Fixture().Build<Partner>().With(x => x.IsActive, false).Without(x => x.PartnerLimits).Create();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, null);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_IsPartnerGivenLimit_NumberIssuedPromoCodesIsZero()
        {
            // Arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var partner = fixture.Build<Partner>().With(x => x.IsActive, true).With(x => x.PartnerLimits).Create();
            partner.PartnerLimits.First().CancelDate = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, new SetPartnerPromoCodeLimitRequest() { Limit = 100, EndDate = DateTime.Now });

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_IsLimitEnded_NumberIssuedPromoCodesNotChanged()
        {
            // Arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var partner = fixture.Build<Partner>().With(x => x.IsActive, true).With(x => x.PartnerLimits).Create();
            var numberIssuedPromoCodesCurrent = partner.NumberIssuedPromoCodes;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, new SetPartnerPromoCodeLimitRequest() { Limit = 100, EndDate = DateTime.Now });

            // Assert
            numberIssuedPromoCodesCurrent.Should().Be(partner.NumberIssuedPromoCodes);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_SetLimitDisablePrevious_CancelDateHasValue()
        {
            // Arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var partner = fixture.Build<Partner>().With(x => x.IsActive, true).With(x => x.PartnerLimits).Create();
            partner.PartnerLimits.First().CancelDate = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, new SetPartnerPromoCodeLimitRequest() { Limit = 100, EndDate = DateTime.Now });

            // Assert
            partner.PartnerLimits.First().CancelDate.Should().HaveValue();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_CheckLimitValue_LimitMoreThanZero()
        {
            // Arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var partner = fixture.Build<Partner>().With(x => x.IsActive, true).With(x => x.PartnerLimits).Create();
            partner.PartnerLimits.First().CancelDate = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, 
                new SetPartnerPromoCodeLimitRequest() { Limit = -1, EndDate = DateTime.Now });

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_CheckUpdateLimit_AddPartnerLimit()
        {
            // Arrange
            var fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var partner = fixture.Build<Partner>().With(x => x.IsActive, true).With(x => x.PartnerLimits).Create();
            partner.PartnerLimits.First().CancelDate = null;

            var newLimit = new SetPartnerPromoCodeLimitRequest() { Limit = 1, EndDate = DateTime.Now.AddMonths(1) };
            var countParnerLimits = partner.PartnerLimits.Count;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, newLimit);

            // Assert
            _partnersRepositoryMock.Verify(x => x.UpdateAsync(It.IsAny<Partner>()), Times.AtLeastOnce);
            partner.PartnerLimits.Count.Should().Be(countParnerLimits + 1);
            partner.PartnerLimits.ElementAt(partner.PartnerLimits.Count - 1).Limit.Should().Be(newLimit.Limit);
            partner.PartnerLimits.ElementAt(partner.PartnerLimits.Count - 1).EndDate.Should().Be(newLimit.EndDate);
        }
    }
}